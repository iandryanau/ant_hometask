import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class BuildClass extends Task {

    private boolean checkdepends;
    private boolean checkdefault;
    private boolean checkdefaultInit;
    private boolean checknames;
    private List<BuildFile> buildFiles = new ArrayList<BuildFile>();

    private final static String VALUE_DEFAULT_ATTR = "main";
    private final static String ATTRIBUTE_DEFAULT = "default";
    private final static String ATTRIBUTE_NAME = "name";
    private final static String ATTRIBUTE_DEPENDS = "depends";
    private final static String TAG_PROGECT = "project";
    private final static String TAG_TARGET = "target";


    @Override
    public void execute() throws BuildException {
        Iterator<BuildFile> iterator = buildFiles.iterator();
        while (iterator.hasNext()) {
            String pathToFile = iterator.next().location;
            InputStream in = null;
            try {
                in = new FileInputStream(pathToFile);
            } catch (FileNotFoundException e) {
                throw new IllegalArgumentException();
            }
            try {
                parse(in);
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }
            System.out.println(this.checkdepends + "; " + this.checkdefault + "; " + this.checknames);
        }
    }

    void parse(InputStream in) throws XMLStreamException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = inputFactory.createXMLStreamReader(in);
        while (reader.hasNext()) {
            int type = reader.next();
            if (type == XMLStreamConstants.START_ELEMENT) {
                if (checkdefaultInit) {
                    checkDefaultAttribute(reader);
                }
                if (checknames) {
                    checkCorrectNames(reader);
                }

                if (checkdepends) {
                    checkDependents(reader, VALUE_DEFAULT_ATTR);
                }

            }
        }

    }

    boolean checkDefaultAttribute(XMLStreamReader reader) {
        checkdefault = true;
        if (reader.getLocalName().equals(TAG_PROGECT) && reader.getAttributeValue(null, ATTRIBUTE_DEFAULT) == null) {
            System.out.println("project doesn't contain default attribute.");
            checkdefault = false;
        }
        return checkdefault;
    }

    boolean checkCorrectNames(XMLStreamReader reader) {
        checknames = true;
        final String REGEX = "[a-zA-Z-]+";
        String attrValue = reader.getAttributeValue(null, ATTRIBUTE_NAME);
        if (attrValue != null && !attrValue.matches(REGEX)) {
            System.out.println("Not correct name:" + reader.getAttributeValue(null, ATTRIBUTE_NAME));
            checknames = false;
        }
        return checknames;
    }

    boolean checkDependents(XMLStreamReader reader, String defaultTarget) {
        checkdepends = true;
        if (reader.getLocalName().equals(TAG_TARGET)) {
            if (reader.getAttributeValue(null, ATTRIBUTE_NAME).equals(defaultTarget)
                    && reader.getAttributeValue(null, ATTRIBUTE_DEPENDS) == null) {
                System.out.println("main doesn't contain dependencies.");
                checkdepends = false;
            }
        }
        return checkdepends;
    }

    public void setCheckdepends(boolean checkdep) {
        this.checkdepends = checkdep;
    }

    public void setCheckdefault(boolean checkdefault) {
        this.checkdefault = checkdefault;
    }

    public void setChecknames(boolean checknames) {
        this.checknames = checknames;
    }

    public static class BuildFile {
        private String location;

        public void setLocation(String location) {
            this.location = location;
        }

        public String getLocation() {
            return this.location;
        }

    }

    public BuildFile createBuildfile() {
        BuildFile buildFile = new BuildFile();
        buildFiles.add(buildFile);
        return buildFile;
    }

    public void setCheckdefaultInit(boolean checkdefaultInit) {
        this.checkdefaultInit = checkdefaultInit;
    }
}